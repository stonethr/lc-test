<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class APIController extends Controller {

    protected $api_base_url;
    protected $api_key;
    protected $api_list_id;
    // lists
    protected $apiV2 = '/api/v2';
    protected $getLists = 'list/';
    protected $listId = 'list/{id}';
    // client instantiation
    protected $http;


    public function __construct() {

        $this->api_key = env('KL_API_KEY');
        $this->api_base_url = env('KL_API_URL');
        $this->api_list_id = env('KL_API_LIST_ID');

    }

    /**
     * @param $details
     * @param $single
     * @return mixed
     */
    protected function _get($details) {

        // set the contact for the list
        $options['api_key'] = $this->api_key;
        $options['emails'] = $details['email'];

        $params = http_build_query($options);
        $url = $this->api_base_url . $this->getLists . $this->api_list_id . '/subscribe?' . $params;

        $response = Http::get($url, $options);
        if(!$response->failed()) {
            return $response->json();
        }

        return $response->failed();

    }

    // single contact create
    protected function _post($details) {
        // set the contact for the list
        $options['api_key'] = $this->api_key;
        $results = $this->_get($details);

        // if the contact exists in klaviyo
        // return ID
        if(!empty($results) && !isset($details['id'])) {
            return $results[0]['id'];
        }

        $options['profiles'] = $details;
        $url = $this->api_base_url . $this->getLists . $this->api_list_id . '/members';

        $response = Http::post($url, $options);

        return $response->json();


    }

    protected function _updatePost($details) {
        // set the contact for the list
        $options['api_key'] = $this->api_key;

        $options['profiles'] = $details;

        $url = $this->api_base_url . $this->getLists . $this->api_list_id . '/members';
//        dd($options);
        $response = Http::post($url, $options);

        return $response->json();
    }

    protected function _postMass($contacts) {

        // set the contact for the list
        $options['api_key'] = $this->api_key;
        $options['profiles'] = $contacts;
        $url = $this->api_base_url . $this->getLists . $this->api_list_id . '/members';
        $response = Http::post($url, $options);

        return $response->json();
    }

    private function checkEmails() {

    }

}
