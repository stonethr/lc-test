<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends APIController {

    public function __construct() {
        parent::__construct();
    }

    /**
     * show members
     * uses paginate
     *
     * @return \Illuminate\Contracts\Foundation\Application
     * |\Illuminate\Contracts\View\Factory
     * |\Illuminate\View\View
     */
    public function index() {
        $Users = new User();

        $users = $Users->getUserMembers();

        return view('members.index')->with('members', $users);
    }

    /**
     * create the new contact
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {

        $request->validate([
            'email' => 'required|email|unique:users',
            'name' => 'required|min:3|max:255'
        ]);

        $User = new User();
        $userDetails = $request->except('_token');
        $userDetails['user_type'] = 'member';
        $createStatus = $User::create($userDetails);

        if($createStatus) {
            $message = 'Successfully created and synced the new contact';
        } else {
            $message = 'Unable to create and sync the new contact';
        }

        return redirect()
            ->route('users-home')
            ->with('status', $message);

    }

    /**
     * update member (user)
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request) {

        // validate request
        $request->validate([
            'email' => 'required|email',
            'name' => 'required|min:3|max:255'
        ]);

        // create user object
        $User = new User();
        $users = $User->getUserMembers();
        // base64 decode user id
        $id = base64_decode($request->input('user_id'));
        // get user by id
        $user = $User->getUserById($id);

        // saving results
        $message = false;
        $errors = [];

        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if ($user->save()) {
            $message = 'Successfully updated the member: ' . $user->name;
        } else {
            $errors = $user->errors();
        }

        return redirect()->route('users-home')
            ->with('members', $users)
            ->with('message', $message)
            ->with('errors', $errors);

    }

    public function edit($id) {
        $decoded_id = base64_decode($id);
        $User = new User();

        $user = $User->getUserById($decoded_id);

        return view('members.edit')
            ->with('edit', true)
            ->with('member', $user);
    }

    public function delete(Request $request) {
        $id = $request->input('');

    }
}
