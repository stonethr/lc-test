<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Contacts;
use Illuminate\Support\Facades\Auth;

class ContactsController extends APIController {

    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index() {
        $Contacts = new Contacts();

        $user_id = null;

        if(Auth::user()->user_type === 'member') {
            $user_id = Auth::user()->user_id;
        }

        $contacts = $Contacts->getAllContacts($user_id);

        return view('contacts.index')
            ->with('message', false)
            ->with('contacts', $contacts);
    }

    public function create() {
        $Contacts = new Contacts();
        $Users = new User();

        $users = $Users->getUserMembersList();

        return view('contacts.edit')
            ->with('message', false)
            ->with('membersList', $users)
            ->with('contact', $Contacts)
            ->with('edit', false);
    }

    public function edit($id) {
        $decoded_id = base64_decode($id);
        $Contacts = new Contacts();

        $contact = $Contacts->getContactById($decoded_id);

        return view('contacts.edit')
            ->with('edit', true)
            ->with('contact', $contact);
    }

    /**
     * create a new contact in both
     * systems
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate([
            'email' => 'required|email|unique:users',
            'first_name' => 'required|min:3|max:255',
            'phone_number' => 'required|min:10|max:12'
        ]);

        $Contact = new Contacts();
        $contactDetails = $request->except('_token');
        $contactDetails['user_id'] = Auth::user()->user_id;
        $apiContactDetails = $request->except(['_token', 'user_id', 'contact_id']);
        $apiContactDetails['phone_number'] = "1" . $apiContactDetails['phone_number'];
        $results = $this->createKlContact($apiContactDetails);

        $apiStatus = true;
        $createStatus = true;
        $id = false;
        // newly created contact
        if(is_array($results)) {
            $id = $results[0]['id'];
        }
        // contact that already exists
        elseif (!is_array($results) && $results) {
            $id = $results;
        }
        // failed to create or return existing contact data
        else {
            $apiStatus = $results;
        }

        // set the kl_contact_id in the system
        $contactDetails['kl_contact_id'] = $id;

        // set the
        if ($apiStatus) {
            $createStatus = $Contact::create($contactDetails);
        }

        if ($createStatus && $apiStatus) {
            $message = 'Successfully created and synced new contact';
        } else {
            $message = 'Unable to create and sync contact';
        }

        return redirect()
            ->route('contacts-home')
            ->with('status', $message);
    }

    public function createKlContact($details) {
        return $this->_post($details);

    }

    public function update(Request $request) {

        $decoded_id = base64_decode($request->input('contact_id'));
        $Contact = new Contacts();

        $apiContactDetails = $request->except(['_token', 'user_id', 'contact_id']);
        if ($apiContactDetails['kl_contact_id'] !== NULL) {
            $apiContactDetails['id'] = $apiContactDetails['kl_contact_id'];
        }

        unset($apiContactDetails['kl_contact_id']);

        $results = $this->updateKlContact($apiContactDetails);

        $apiStatus = true;
        $updateStatus = true;
        $id = false;
        // newly created contact
        if(is_array($results)) {
            $id = $results[0]['id'];
        }
        // contact that already exists
        elseif (!is_array($results) && $results) {
            $id = $results;
        }
        // failed to create or return existing contact data
        else {
            $apiStatus = $results;
        }

        // find the contact in DB
        if ($apiStatus) {
            $contact = $Contact->find($decoded_id);
            if ($id) {
                $contact->kl_contact_id = $id;
            }
            $contact->first_name = $request->input('first_name');
            $contact->email = $request->input('email');
            $contact->phone_number = $request->input('phone_number');

            $updateStatus = $contact->save();
        }

        if($updateStatus) {
            $message = 'Successfully updated the contact';
        } else {
            $message = 'Unable to update the contact';
        }

        return redirect()
            ->route('contacts-home')
            ->with('status', $message);

    }

    private function updateKlContact($details) {
        return $this->_updatePost($details);
    }

    public function loadCSV() {
        $Users = new User();

        $users = $Users->getUserMembersList();

        return view('contacts.loadcsv')
            ->with('membersList', $users);
    }

    public function createFromSCsv(Request $request) {
        $file = $request->file('file');
        $user_id = Auth::user()->user_id;

        $contacts = [];

        $handle = fopen($file->getRealPath(), 'r');
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            if ($data[0] !== 'first_name') {
                array_push($contacts, [
                    'first_name' => $data[0],
                    'email' => $data[1],
                    'phone_number' => $data[2]
                ]);
            }
        }
        fclose($handle);

        $apiResults = $this->multipleCreateContacts($contacts);

        if(isset($apiResults['detail'])) {
            return redirect()
                ->route('home')->with(['error', 'unable to load file to klaviyo: error [' . $apiResults['detail'] . ']' ]);
        }

        foreach($apiResults as $index =>$result) {
            $contacts[$index]['kl_contact_id'] = $result['id'];
            $contacts[$index]['user_id'] = $user_id;
        }

        $Contacts = new Contacts();
        if ($Contacts->insert($contacts)) {
            return redirect()
                ->route('home')->with(['status', 'The contacts have been saved and synced!' ]);
        }

        return redirect()
            ->route('home')->with(['error', 'Many things have gone wrong, please reconsider this file']);

    }

    private function multipleCreateContacts($contacts) {
        return $this->_postMass($contacts);
    }

    /**
     * delete contact by Id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request) {
        $id = $request->input('id');

        $Contacts = new Contacts();

        $status = $Contacts->deleteContactById($id);

        if($status) {
            $message = 'deleted';
        } else {
            $message = 'error';
        }

        return response()->json(['status' => $message]);
    }

}
