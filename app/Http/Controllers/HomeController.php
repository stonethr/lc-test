<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class HomeController extends Controller {

    protected $api_key;
    private $host = 'https://a.klaviyo.com/';
    protected $TRACK_ONCE_KEY = '__track_once__';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->api_key = env('KL_API_KEY');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {

        // get user type
        $user_type = Auth::user()->user_type;

        // create user obj
        $User = new User();

        // get user by Id
        //if user type is member
        if($user_type === 'member') {
            $user = $User->getUserById(Auth::user()->user_id);
        } else {
            $user = $User;
        }

        return view('home.index')
            ->with('member', $user)
            ->with('user_type', $user_type);
    }

    public function track() {

        $timestamp = time();

        $params = array(
            'token' => $this->api_key,
            'event' => "btn track",
            'properties' => ['tracking' => 'laravel click button'],
            'customer_properties' => ['email' => 'track@it.com']
        );

        if (!is_null($timestamp)) {
            $params['time'] = $timestamp;
        }

        $encoded_params = $this->build_params($params);
        return $this->make_request('api/track', $encoded_params);
    }

    protected function build_params($params) {
        return 'data=' . urlencode(base64_encode(json_encode($params)));
    }

    protected function make_request($path, $params) {
        $url = $this->host . $path . '?' . $params;
        $response = file_get_contents($url);

        return $response == 1;
    }

}
