<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_type', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function contacts() {
        return $this->hasMany(Contacts::class,'user_id');
    }

    public function getUserMembers() {
        return $this->where('user_type', 'member')->paginate(10);
    }

    public function getUserMembersList() {
        $users =  $this->where('user_type', 'member')->get();
        $collection = collect($users)->pluck('name', 'user_id');

        return $collection;

    }

    public function getUserById($id) {
        return $this->where('user_id', $id)->with('contacts')->first();
    }
}
