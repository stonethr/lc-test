<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model {
    protected $fillable = ['first_name', 'user_id', 'email', 'phone_number', 'kl_contact_id'];

    protected $primaryKey = 'contact_id';

    protected $table = 'contacts';

    /**
     * contact belongs to member
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * get all contacts
     *
     * @return mixed
     */
    public function getAllContacts($id) {
        $query = $this->select('*');

        if($id !== NULL) {
            $query = $query->where('user_id', $id);
        }

        return $query->paginate(10);
    }

    /**
     * get contact by ID
     *
     * @param $id
     * @return mixed
     */
    public function getContactById($id) {
        return $this->where('contact_id', $id)->first();
    }

    public function deleteContactById($id) {
        return $this->destroy($id);
    }
}
