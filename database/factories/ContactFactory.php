<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contacts;
use Faker\Generator as Faker;

$factory->define(Contacts::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(2, 10),
        'first_name' =>$faker->firstName,
        'phone_number' => '15613154287',
        'email' => $faker->unique()->safeEmail,
    ];
});
