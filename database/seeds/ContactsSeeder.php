<?php

use Illuminate\Database\Seeder;
use App\Contacts;

class ContactsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        factory(Contacts::class, 100)->create();
    }
}
