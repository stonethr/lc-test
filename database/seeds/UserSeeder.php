<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $user = factory(User::class, 10)->create()->each(function ($u, $i) {
            if ($i === 0) {
                $u->name = 'admin';
                $u->email = 'admin@test.com';
                $u->user_type = 'admin';
            }
            $u->save();
        });
    }
}
