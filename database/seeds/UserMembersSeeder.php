<?php

use Illuminate\Database\Seeder;
use App\User;

class UserMembersSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        factory(UserMembers::class, 1)->create();
    }
}
