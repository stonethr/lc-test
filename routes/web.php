<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/track-it','HomeController@track')->name('track-it');

// contacts routes
Route::group(['prefix' => '/contacts'], function () {
    Route::get('/', 'ContactsController@index')->name('contacts-home');
    Route::get('/create', 'ContactsController@create')->name('contact-create');
    Route::get('/load', 'ContactsController@loadCSV')->name('contacts-load');
    Route::get('/edit/{id}', 'ContactsController@edit')->name('contact-edit');
    Route::post('/store', 'ContactsController@store')->name('contact-store');
    Route::post('/createFromSCsv', 'ContactsController@createFromSCsv')->name('contact-load-create');
    Route::post('/update', 'ContactsController@update')->name('contact-update');
    Route::post('/delete', 'ContactsController@delete')->name('contact-delete');
});

// members routes
Route::group(['prefix' => '/users'], function () {
    Route::get('/', 'UsersController@index')->name('users-home');
    Route::get('/edit/{id}', 'UsersController@edit')->name('user-edit');
    Route::get('/users/{id}/contacts', 'UsersController@contacts')->name('member-contacts-view');
    Route::post('/users/contacts/store', 'ContactsController@store')->name('member-contacts-view');
    Route::post('/store', 'UsersController@store')->name('user-store');
    Route::post('/update', 'UsersController@update')->name('user-update');
    Route::post('/delete/', 'UsersController@delete')->name('user-delete');
});



