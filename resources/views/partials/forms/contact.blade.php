<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(!$edit)
                    <div class="card-header">{{ __('Create') }}</div>
                @else
                    <div class="card-header">{{ __('Edit') }}</div>
                @endif

                <div class="card-body">
                    @if(!$edit)
                        {!! Form::open(['route' => 'contact-store']) !!}

                        @if(Auth::user()->user_type === 'admin')
                        <div class="form-group row">
                            {!! Form::label('user_id', 'User', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            <div class="col-md-6">
                                {!! Form::select('user_id', $membersList, null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        @endif
                    @else
                        {!! Form::open(['route' => 'contact-update']) !!}

                        <div class="form-group row">
                            {!! Form::label('kl_contact_id', 'KL Contact ID', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            <div class="col-md-6">
                                {!! Form::text('kl_contact_id', $contact['kl_contact_id'], ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                            </div>
                        </div>

                        {!! Form::hidden('contact_id', base64_encode($contact['contact_id'])) !!}
                        {!! Form::hidden('kl_contact_id', $contact['kl_contact_id']) !!}
                    @endif

                        @csrf

                        <div class="form-group row">
                            {!! Form::label('first_name', 'First Name', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            <div class="col-md-6">
                                {!! Form::text('first_name', $contact['first_name'], ['class' => 'form-control']) !!}
                                @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('email', 'Email', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                {!! Form::email('email', $contact['email'], ['class' => 'form-control']) !!}

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('phone_number', 'Phone', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                {!! Form::text('phone_number', $contact['phone_number'], ['class' => 'form-control']) !!}

                                @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    @if(!$edit)
                                        {{ __('Save') }}
                                    @else
                                        {{ __('Update') }}
                                    @endif
                                </button>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
