<div class="container">
    @if (session('status'))
        <div class="mb-5 mt-5"></div>
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="mb-5 mt-5"></div>
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="mb-5 mt-5"></div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit') }}</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(isset($edit))
                        {!! Form::open(['route' => 'user-store']) !!}
                    @else
                        {!! Form::open(['route' => 'user-update']) !!}
                            {!! Form::hidden('user_id', base64_encode($member['user_id'])) !!}
                    @endif

                    @csrf

                    <div class="form-group row">
                        {!! Form::label('name', 'Name', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                        <div class="col-md-6">
                            {!! Form::text('name', $member['name'] ?? '', ['class' => 'form-control']) !!}
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {!! Form::label('email', 'Email', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                        <div class="col-md-6">
                            {!! Form::email('email', $member['email'], ['class' => 'form-control']) !!}

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="mb-5 mt-5"></div>
    <div class="row justify-content-end">
        <div class="col-lg-4">
            <a href="{{ route('contact-create') }}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> Create Contact</a>
        </div>
    </div>
    <div class="mb-5 mt-5"></div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>KL Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($member['contacts'] as $contact)
                    @include('contacts.row', $contact)
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{--<div class="row justify-content-end">
        {!! $contacts->render() !!}
    </div>--}}
</div>

