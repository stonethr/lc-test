@extends('layouts.app')

@section('content')

    @if($user_type === 'admin')
        @include('home.admin')
    @else
        @include('home.user')
    @endif

@endsection



