@extends('layouts.app')

@section('content')

    <div class="mb-5 mt-5"></div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="mb-5 mt-5"></div>
    <div class="row justify-content-center">
        <div class="col-lg-4">
            <h2 class="text-center">Contacts</h2>
        </div>
    </div>
    <div class="mb-5 mt-5"></div>
    <div class="row justify-content-end">
        <div class="col-lg-4">
            <a href="{{ route('contact-create') }}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> Create Contact</a>
        </div>
    </div>
    <div class="mb-5 mt-5"></div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>KL Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contacts as $contact)
                        @include('contacts.row', $contact)
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row justify-content-end">
        {!! $contacts->render() !!}
    </div>
@endsection
@section('inline-js')
    <script type="application/javascript">
        $("a[id^='del-']").on('click', function (e) {
            e.preventDefault();

            var fullId = $(this).attr('id');
            var idChunks = fullId.split('-');
            var id = idChunks[1];

            if (confirm ("Delete?")) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('contact-delete') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id: id
                    },
                    success: function (response) {
                        if (response.status === 'deleted') {
                            $('#tr-' + id).fadeOut(1000, function() {
                                $('#tr-' + id).remove();
                            });
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //On error, we alert user
                        alert(thrownError);
                    }
                });
            }
        });

        $

    </script>
@endsection
