@extends('layouts.app')

@section('content')

    <div class="row">
        @include('partials.forms.contact')
    </div>

@endsection

@section('inline-js')
    <script type="application/javascript">
    </script>
@endsection
