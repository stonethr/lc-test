@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Upload') }}</div>

                    <div class="card-body">
                            {!! Form::open(['route' => 'contact-load-create', 'files' => true]) !!}

                            @if(Auth::user()->user_type === 'admin')
                                <div class="form-group row">
                                    {!! Form::label('user_id', 'User', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                                    <div class="col-md-6">
                                        {!! Form::select('user_id', $membersList, null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                            @else

                            {!! Form::hidden('user_id', base64_encode(Auth::user()->user_id)) !!}
                        @endif

                        @csrf

                        <div class="form-group row">
                            {!! Form::label('upload_csv', 'Upload CSV File', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            <div class="col-md-6">
                                {!! Form::file('file', ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Upload') }}
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
