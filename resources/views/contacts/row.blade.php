<tr id="tr-{{ $contact_id }}">
    <td>{{ $kl_contact_id }}</td>
    <td>{{ $first_name }}</td>
    <td>{{ $email }}</td>
    <td>{{ $phone_number }}</td>
    <td><a class="btn btn-primary" href="{!! route('contact-edit', base64_encode($contact_id)) !!}"><i class="fa fa-edit"></i></a></td>
    <td><a id="del-{{ $contact_id }}" class="btn btn-danger" href="{!! route('contact-delete') !!}"><i class="fa fa-trash-alt"></i></a></td>
</tr>
