<!-- ======= Footer ======= -->
<footer id="footer" class="footer mt-auto py-3">

    <div class="container clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>LC Saas Contact</span></strong>. All Rights Reserved
        </div>

    </div>
</footer><!-- End Footer -->

@section('inline-js')

@endsection
