<tr id="tr-{{ base64_encode($user_id) }}">
    <td>{{ $name }}</td>
    <td>{{ $email }}</td>
    <td><a class="btn btn-primary" href="{!! route('user-edit', base64_encode($user_id)) !!}"><i class="fa fa-edit"></i></a></td>
    <td><a id="del-{{base64_encode($user_id)}}" class="btn btn-danger" href="{!! route('user-delete', base64_encode($user_id)) !!}"><i class="fa fa-trash-alt"></i></a></td>
</tr>
