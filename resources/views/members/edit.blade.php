@extends('layouts.app')

@section('content')

    <div class="row">
        @include('partials.forms.member')
    </div>

@endsection

@section('inline-js')
    <script type="application/javascript">
        $("a[id^='del-']").on('click', function (e) {
            e.preventDefault();

            var fullId = $(this).attr('id');
            var idChunks = fullId.split('-');
            var id = idChunks[1];

            if (confirm ("Delete?")) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('contact-delete') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id: id
                    },
                    success: function (response) {
                        if (response.status === 'deleted') {
                            $('#tr-' + id).fadeOut(1000, function() {
                                $('#tr-' + id).remove();
                            });
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //On error, we alert user
                        alert(thrownError);
                    }
                });
            }
        });

        $

    </script>
@endsection
{{--4,2,,Cordie,hoppe.alexanne@example.com,738-260-1110--}}

