@extends('layouts.app')

@section('content')
    <div class="mb-5 mt-5"></div>
    <div class="row justify-content-center">
        <div class="col-lg-4">
            <h2 class="text-center">Members</h2>
        </div>
    </div>
    <div class="mb-5 mt-5"></div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($members as $member)
                    @include('members.row', $member)
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row justify-content-end">
        {!! $members->render() !!}
    </div>
@endsection
